/**
* @file Dugum.cpp
* @description Dugum sınıfına ait fonksiyonların gövdelerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  4.Ödev
* @date 12.12.2013
*@author MELİH GÖKSAL,SELİMHAN UZAKTAŞ,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr,selimhan.uzaktas@ogr.sakarya.edu.tr,eray.alakese@gr.sakarya.edu.tr
*/
#include "Dugum.hpp"
#include <iostream>
using namespace std;
Dugum::Dugum(string isim)
{
	solDugum = NULL;
	sagDugum = NULL;
	veri = new Insan(isim);
}
Dugum::Dugum()
{
	solDugum = NULL;
	sagDugum = NULL;
	veri = NULL;
}
Dugum* Dugum::getSoldaki()
{
	return solDugum;
}
void Dugum::setSoldaki(Dugum* sol)
{
	solDugum = sol;
}
Dugum* Dugum::getSagdaki()
{
	return sagDugum;
}
void Dugum::setSagdaki(Dugum* sag)
{
	sagDugum = sag;
}
Insan* Dugum::getVeri()
{
	return veri;
}
void Dugum::setVeri(Insan* v)
{
	veri=v;
}
int Dugum::getLevel()
{
	return level;
}
void Dugum::setLevel(int l)
{
	level = l;
}