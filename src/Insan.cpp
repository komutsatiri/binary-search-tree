/**
* @file Insan.cpp
* @description Insan sınıfına ait fonksiyonların gövdelerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  4.Ödev
* @date 12.12.2013
*@author MELİH GÖKSAL,SELİMHAN UZAKTAŞ,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr,selimhan.uzaktas@ogr.sakarya.edu.tr,eray.alakese@gr.sakarya.edu.tr
*/
#include "Insan.hpp"
#include <iostream>
#include <fstream>
#include <time.h>
#include <cstdlib>
using namespace std;
Insan::Insan(string isim)
{

   this->isim = isim;
   
   long random2 = rand()%76543210889 + 23456789011 ;
   this->tcKimlik = random2;

}
string Insan::getIsim()
{
   return isim;
}
void Insan::setIsim(string i)
{
   isim = i;
}