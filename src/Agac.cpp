/**
* @file Agac.cpp
* @description Agac sınıfına ait fonksiyonların gövdelerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  4.Ödev
* @date 12.12.2013
*@author MELİH GÖKSAL,SELİMHAN UZAKTAŞ,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr,selimhan.uzaktas@ogr.sakarya.edu.tr,eray.alakese@gr.sakarya.edu.tr
*/
#include "Agac.hpp"
#include "Dugum.hpp"
#include <iostream>
#include <fstream>
#include <time.h>
#include <cstdlib>
#include <iomanip>   
#include <cmath>
#include <queue>
using namespace std;

Agac::Agac()
{

  for(int i=0;i<16;i++)
    dizi[i] = 0;

	string isim;
   int i=0, random1;
   srand(time(NULL));

   ifstream myfile("./isimler.txt");
  	if (myfile.is_open())
  	{
      while ( myfile.good() )
      {
         
         myfile >> isimler[i];
         i++;
      }      
      myfile.close();
   }        
	else 
      cout << "Unable to open file\n"; 


  	random1 = rand()%49 +1 ;
  	while(isimler[random1] == "")
  	{
   		random1 = rand()%49 +1 ;
   	}
   	isim = isimler[random1];
   	isimler[random1] = "";
   	
    Dugum* dugum = new Dugum(isim);
    root = dugum;
    root->setLevel(1);
}

void Agac::ekle(string isim)
{
  /*
    Algoritma:
    1) root'a git
    2) karşılaştır ve sağ sol karar ver
    3) Karar verilen yönde başka düğüm var mı? Varsa 4. adım yoksa X
    4) sağ yada sola git
    5) 2. adıma git
    6) Düğümü ekle 
  */

  /*
  Düğüm level'ını belirlerken dugum değişkeninin level'ına bakılıp, o 1 arttırılarak yeniDugum->setLevel() yapılabilir
  */
  int level =1;
  Dugum* yeniDugum = new Dugum(isim);
  Dugum* dugum = root;
  while(dugum != NULL)
  {
    
    if(isim > dugum->getVeri()->getIsim())
    {
      // büyükse
      cout << isim << dugum->getVeri()->getIsim() <<"den" << "buyuk"<<endl;
      if(dugum->getSagdaki()==NULL)
      {
        break;
      }
      else
      {
        dugum = dugum->getSagdaki();
        level++;
        cout << level<<endl;
      }
    }
    else
    {
      // küçükse
      cout << isim << dugum->getVeri()->getIsim() <<"den" << "kucuk"<<endl;
      if(dugum->getSoldaki()==NULL)
      {
        break;
      }
      else
      {
        dugum = dugum->getSoldaki();
        level++;
        cout << level<<endl;
      }
    }
  }
  // Bu noktada dugum en uç düğümü tutuyor.
  int l=0; 
  if(isim > dugum->getVeri()->getIsim())
  {
    dugum->setSagdaki(yeniDugum);
    dugum->setLevel(level);
    
    insert(level);
  }
  else
  {
    dugum->setSoldaki(yeniDugum);
    dugum->setLevel(level);
    

    insert(level);
  }

}
void Agac::insert(int deger)
{
  int i = 0;
  while(dizi[i] != 0)
  {
    i++;
  }
  dizi[i] = deger;
 
}
int Agac::maxlevel()
{

 int enBuyuk = dizi[0]; 
 for(int i=1;i<16;i++)
 {
    if(dizi[i] > enBuyuk)
      enBuyuk = dizi[i];
      

 }

 return enBuyuk;
}
void Agac::yazdir()
{
  Dugum* d;
  cout << endl << "########" << root->getVeri()->getIsim()<<endl;
  cout << "Solunda : ";(root->getSoldaki()==NULL?cout<<endl:cout<<root->getSoldaki()->getVeri()->getIsim()<<endl);
  cout << "Saginda : ";(root->getSagdaki()==NULL?cout<<endl:cout<<root->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << root->getLevel()<<endl;

  if(root->getSoldaki() != NULL)
  {
    d = root->getSoldaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
    cout << "Level : " << d->getLevel()<<endl;
  }

  if(root->getSagdaki() != NULL)
  {
    d = root->getSagdaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }

  if(root->getSoldaki() != NULL && root->getSoldaki()->getSoldaki() != NULL)
  {
    d = root->getSoldaki()->getSoldaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSoldaki() != NULL && root->getSoldaki()->getSagdaki() != NULL)
  {
    d = root->getSoldaki()->getSagdaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }

  if(root->getSagdaki() != NULL && root->getSagdaki()->getSoldaki() != NULL)
  {
    d = root->getSagdaki()->getSoldaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSagdaki() != NULL && root->getSagdaki()->getSagdaki() != NULL)
  {
    d = root->getSagdaki()->getSagdaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }

 
  if(root->getSoldaki() != NULL && root->getSoldaki()->getSoldaki() != NULL && root->getSoldaki()->getSoldaki()->getSoldaki() != NULL)
  {
    d = root->getSoldaki()->getSoldaki()->getSoldaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSoldaki() != NULL && root->getSoldaki()->getSoldaki() != NULL && root->getSoldaki()->getSagdaki()->getSoldaki() != NULL)
  {
    d = root->getSoldaki()->getSoldaki()->getSagdaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSoldaki() != NULL && root->getSoldaki()->getSagdaki() != NULL && root->getSoldaki()->getSagdaki()->getSoldaki() != NULL)
  {
    d = root->getSoldaki()->getSagdaki()->getSagdaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSoldaki() != NULL && root->getSoldaki()->getSagdaki() != NULL && root->getSoldaki()->getSagdaki()->getSagdaki() != NULL)
  {
    d = root->getSoldaki()->getSagdaki()->getSagdaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSagdaki() != NULL && root->getSagdaki()->getSoldaki() != NULL && root->getSagdaki()->getSoldaki()->getSoldaki() != NULL)
  {
    d = root->getSagdaki()->getSoldaki()->getSoldaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSagdaki() != NULL && root->getSagdaki()->getSoldaki() != NULL && root->getSagdaki()->getSoldaki()->getSagdaki() != NULL)
  {
    d = root->getSagdaki()->getSoldaki()->getSagdaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSagdaki() != NULL && root->getSagdaki()->getSagdaki() != NULL && root->getSagdaki()->getSagdaki()->getSoldaki() != NULL)
  {
    d = root->getSagdaki()->getSagdaki()->getSoldaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
  if(root->getSagdaki() != NULL && root->getSagdaki()->getSagdaki() != NULL && root->getSagdaki()->getSagdaki()->getSagdaki() != NULL)
  {
    d = root->getSagdaki()->getSagdaki()->getSagdaki();
    cout << endl << "########" << d->getVeri()->getIsim()<<endl;
    cout << "Solunda : ";(d->getSoldaki()==NULL?cout<<endl:cout<<d->getSoldaki()->getVeri()->getIsim()<<endl);
    cout << "Saginda : ";(d->getSagdaki()==NULL?cout<<endl:cout<<d->getSagdaki()->getVeri()->getIsim()<<endl);
  cout << "Level : " << d->getLevel()<<endl;
 }
}
void Agac::printWhitespaces(int count) 
{
        for (int i = 0; i < count; i++)
            cout<<" ";
}
void Agac::yazdirbaba()
{
  cout<<"maxlevel: "<<maxlevel()<<endl; 
  Dugum* d = root;
  
  int sol=1, sag=1,maxLevel,first,between;
  maxLevel=maxlevel();
  queue<Dugum*> q;

  first = pow(2, maxLevel-d->getLevel());
  
  printWhitespaces(first);

    cout << d->getVeri()->getIsim();

    if(d->getSoldaki() != NULL) 
      q.push(d->getSoldaki()); 
    else 
      q.push(new Dugum());
    if(d->getSagdaki() != NULL) 
      q.push(d->getSagdaki());  
    else 
      q.push(new Dugum());
    cout << endl;
  

  for(int level=2;level<=maxLevel;level++)
  {
    first= pow(2,level)*7/10; 
    between = pow(2, maxLevel-level);
    printWhitespaces(first);

  for(int e = 1;e<=pow(2,level-1);e++)
  {
    if(q.front() != NULL && q.front()->getVeri() != NULL)
    {
        cout<<q.front()->getVeri()->getIsim();
        if(q.front()->getSoldaki() != NULL)
          q.push(q.front()->getSoldaki());
        else
          q.push(new Dugum());
        if(q.front()->getSagdaki() != NULL)
          q.push(q.front()->getSagdaki());
        else
          q.push(new Dugum());
     }
    else cout<<" ";
      q.pop();
    printWhitespaces(between);
  }
  cout << endl;
  }

}