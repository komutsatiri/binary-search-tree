/**
* @file Agac.hpp
* @description Agac sınıfına ait fonksiyon prototiplerini ve sınıf değişkenlerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  4.Ödev
* @date 12.12.2013
*@author MELİH GÖKSAL,SELİMHAN UZAKTAŞ,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr,selimhan.uzaktas@ogr.sakarya.edu.tr,eray.alakese@gr.sakarya.edu.tr
*/
#ifndef AGAC_HPP
#define AGAC_HPP
#include "Dugum.hpp"
class Agac
{
private:
	
public:
	Dugum* root;
	void ekle(string isim);
	void yazdir();		
	Agac();
	string isimler[50]; //Dosyadan okunan isimlerin tutulduğu global değişken
	void printNode();
	int maxlevel();
	int max(int a, int b);
	void printWhitespaces(int count);
	void yazdirbaba();
	int dizi[16];
	void insert(int);
	
;
};
#endif
