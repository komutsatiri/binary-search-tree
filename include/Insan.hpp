/**
* @file Insan.hpp
* @description Insan sınıfına ait fonksiyon prototiplerini ve sınıf değişkenlerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  4.Ödev
* @date 12.12.2013
*@author MELİH GÖKSAL,SELİMHAN UZAKTAŞ,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr,selimhan.uzaktas@ogr.sakarya.edu.tr,eray.alakese@gr.sakarya.edu.tr
*/
#ifndef INSAN_HPP
#define INSAN_HPP
#include <string>
using namespace std;
class Insan
{
private:
	long tcKimlik;
	string isim;
	
public:
	Insan(string isim);
	string getIsim();
	void setIsim(string i);
	bool	operator>(Insan);
	bool	operator<(Insan);
	bool	operator==(Insan);
	
};
#endif