/**
* @file Dugum.hpp
* @description Dugum sınıfına ait fonksiyon prototiplerini ve sınıf değişkenlerini içerir.
* @course 2.Öğretim B Grubu
* @assignment  4.Ödev
* @date 12.12.2013
*@author MELİH GÖKSAL,SELİMHAN UZAKTAŞ,ERAY ALAKESE melih.goksal@ogr.sakarya.edu.tr,selimhan.uzaktas@ogr.sakarya.edu.tr,eray.alakese@gr.sakarya.edu.tr
*/
#ifndef DUGUM_HPP
#define DUGUM_HPP
#include "Insan.hpp"
class Dugum
{
private:
	Insan* veri;
	Dugum* solDugum;	
	Dugum* sagDugum;
	int level;
public:
	Dugum(string isim);
	Dugum();
	~Dugum();
	Dugum* getSoldaki();
	void setSoldaki(Dugum* sol);
	Dugum* getSagdaki();
	void setSagdaki(Dugum* sag);
	Insan* getVeri();
	void setVeri(Insan* veri);
	int getLevel();
	void setLevel(int l);
};
#endif