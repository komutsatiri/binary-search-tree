all: compile execute

compile:
	g++ -I ./include/ -o ./lib/Insan.o -c ./src/Insan.cpp
	g++ -I ./include/ -o ./lib/Dugum.o -c ./src/Dugum.cpp
	g++ -I ./include/ -o ./lib/Agac.o -c ./src/Agac.cpp
	g++ -I ./include/ -o ./bin/test ./lib/Agac.o ./lib/Insan.o ./lib/Dugum.o ./src/main.cpp
execute:
	./bin/test
remove:
	rm -rf ./bin/* ./lib/*		